# Implementation Gillespie Algorithm

A simple python implementation of the Gillespie Algorithm

## Dependences
- click
- matplotlib
- numpy

### Arguments
| Parameter        |Type   | Description   | Example     |
|:-----------------|:-----:|:-------------:|:------------|
| --help           | None  |Help| |
|-b --beta        |str |Reaction equation | "2, -1, 0, 0, 0; -1, 0, 1, -1,1"
|-m --molecules   |str |Molecules distribution at t0 | "20, 30, 100, 10, 40"
|-i --initial_time|int |Initial time | 0
|-c --rate        |str |Reaction rate|"1,2,3,4,5"
|-f --final_time  |int |Final time   | 3
|-n --names       |str |Molecules names |"A,B,C,D,E" 


![Image](https://gitlab.com/matteounimib/biocomp/raw/master/reaction.png) "Molecules population in time"