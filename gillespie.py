import math
import random
from functools import reduce


def h(x, eq):
    tot = 1
    for i,j in zip(eq, x):
        if i<0:
            tot *= j

    return tot


def choice(a, a0, r2):
    tot = 0
    for i in range(len(a)):
        tot += a[i]
        if tot >= a0*r2:
            return i
    return -1

# beta : matrix ; reactions equations
# x0   : lis    ; system initial status (molecules population)
# t0   : int    ; initial time
# c    : list   ; rate of reactions
# t_f  : int    ; final time
def gillespie(beta, x0, t0, c, t_f):
    x,t = x0,t0
    # initializate output matrix
    result = []
    for i in x:
        result.append([])

    while t<t_f:
        a = []

        # step 1
        # evaluate all a_j(x)

        for i,j in zip(beta, c):
            a.append(h(x, i)*j)

        # evaluate a0
        a0 = reduce(lambda x,y: x+y, a)
        if a0 == 0:
            print("no more molecules to react")
            print("time t: ", t)
            return result
        # step 2
        r1 = random.uniform(0,1)
        r2  = random.uniform(0,1)
        tau = 1/(a0) * math.log(1/r1)


        # step 3
        choiceR = choice(a, a0, r2)
        if choiceR == -1:
            print("no possible reaction")
            print("time t: ", t)
            return result

        x = [(lambda a, b: a+b)(a,b) for (a,b) in zip(x,beta[choiceR])]
        t += tau

        print(x)
        for i,j in zip(result, x):
            i.append(j)
    print("time t: ", t)

    return result
