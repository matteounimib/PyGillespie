import click
import matplotlib.pyplot as plt
import numpy as np
from sys import argv

from gillespie import gillespie

@click.command()
@click.option("-b", "--beta", help="Reaction Equations", type=str)
@click.option("-m", "--molecules", help="Molecules distribution at t0", type=str)
@click.option("-i", "--initial_time", help="Initial time", type=int)
@click.option("-c", "--rate", help="Rate of Reaction", type=str)
@click.option("-f", "--final_time", help="Final time", type=int)
@click.option("-n", "--names", help="Molecules Names", type=str)
@click.option("-t", "--times", help="Number of runs",  type=int, default=1)

def main(beta, molecules, initial_time, rate, final_time, names, times):
    toPlot = {}
    beta.strip()
    matrix = []
    beta=beta.replace(" ","").split(";")
    molecules=molecules.replace(" ","").split(",")
    molecules=list(map(int, molecules))
    rate = rate.replace(" ","").split(",")
    rate = list(map(float, rate))
    nm = names.split(" ")
    for i in beta:
        matrix.append(i.split(","))

    beta = []
    for i in matrix:
        beta.append(list(map(int, i)))

    for i in nm:
        toPlot[i] = []

    # simpler example
    # data =  gillespie([[-2, -1, 2],[-3,4,0]],[110,20,110], 0, [1, 1], 10)

    # br br2 h h2 hbr
    # o + c -> r
    # cl2 -> 2cl -> 1
    # data = gillespie([[2, -1, 0, 0, 0], [-1, 0, 1, -1, 1],
    #                   [1, -1, -1, 0, 1], [-2, 1, 0, 0, 0],
    #                   [0, 0,-2,1, 0]],[0,100,0,100,0], 0, [1,1,0.4,1,1], 10)
    for i in range(times):
        data = gillespie(beta, molecules, initial_time, rate, final_time)
        x = np.arange(len(data[0]))
        color = i
        for d,t in zip(data, toPlot):
            toPlot[t].append(d)
            plt.plot(x, d, color)

    plt.legend(nm)
    plt.show()
if __name__ == '__main__':
    try:
        main()
    except (EOFError, KeyboardInterrupt):
        pass
